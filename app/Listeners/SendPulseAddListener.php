<?php

namespace App\Listeners;

use App\Events\SendExceptionEvent;
use App\Events\SendPulseAddEvent;
use App\Service\SendPulse\SendPulseService;
use App\Service\UpdateStudentNotificationService;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendPulseAddListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param SendPulseAddEvent $event
     */
    public function handle(SendPulseAddEvent $event)
    {
        try
        {
            $student = $event->getStudent();
            $service = new SendPulseService();
            foreach ($event->getBookTypes() as $key => $bookType){
                if(!issetInSendPulse($student, $bookType)) {
                    $responseApi = $service->subscribeUser($student->firstname, $student->email, $event->getLang(), $bookType);
                    if ($responseApi->result == true){
                        $updateNotificationSettingService = new UpdateStudentNotificationService($student);
                        $updateNotificationSettingService->updateFieldByBookType($bookType, 1);
                    }
                }
            }
        }catch (\Exception $exception)
        {
            event(new SendExceptionEvent($exception));
        }
    }
}
