<?php

namespace App\Events;

use App\Models\Student;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class SendPulseRemoveEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    protected $student;
    protected $lang;
    protected $bookTypes;

    /**
     * Create a new event instance.
     *
     * SendPulseRemoveEvent constructor.
     *
     * @param $params
     */
    public function __construct($params)
    {
        $this->student = $params->student;
        $this->bookTypes = $params->bookTypes;
        $this->lang = (!empty($params->lang)) ? $params->lang : $this->student->getNotificationLanguage();
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }

    /**
     * @return Student
     */
    public function getStudent(): Student
    {
        return $this->student;
    }

    /**
     * @return string
     */
    public function getLang()
    {
        return $this->lang;
    }

    public function getBookTypes()
    {
        return $this->bookTypes;
    }
}
