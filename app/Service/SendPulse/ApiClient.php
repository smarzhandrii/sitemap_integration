<?php

namespace App\Service\SendPulse;

use Sendpulse\RestApi\ApiClient as ApiClientSendPulse;


class ApiClient extends ApiClientSendPulse
{
    /**
     * Unsubscribe emails to address book
     *
     * @param $bookID
     * @param $emails
     *
     * @return \stdClass
     */
    public function unsubscribeEmails($bookID, $emails)
    {
        if (empty($bookID) || empty($emails)) {
            return $this->handleError('Empty book id or emails');
        }

        $data = array(
            'emails' => json_encode($emails),
        );

        $requestResult = $this->sendRequest('addressbooks/' . $bookID . '/emails/unsubscribe', 'POST', $data);

        return $this->handleResult($requestResult);
    }
}
