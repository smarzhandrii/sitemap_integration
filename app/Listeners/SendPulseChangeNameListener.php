<?php

namespace App\Listeners;

use App\Events\SendExceptionEvent;
use App\Events\SendPulseChangeNameEvent;
use App\Service\SendPulse\SendPulseService;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendPulseChangeNameListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(SendPulseChangeNameEvent $event)
    {
        try{
            $student = $event->getStudent();
            $service = new SendPulseService();
            $bookTypes = $service->getStudentAddressBooks($student);

            if($bookTypes){
                foreach ($bookTypes as $bookType){
                    $service->updateUserName($student->email, $bookType, $event->getName());
                }
            }
        }catch (\Exception $exception)
        {
            event(new SendExceptionEvent($exception));
        }
    }
}
