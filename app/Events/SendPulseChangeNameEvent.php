<?php

namespace App\Events;

use App\Models\Student;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class SendPulseChangeNameEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    protected $student;
    protected $name;

    /**
     * SendPulseChangeNameEvent constructor.
     *
     * Create a new event instance.
     *
     * @param $params
     */
    public function __construct($params)
    {
        $this->student = $params->student;
        $this->name = $params->name;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }

    /**
     * @return Student
     */
    public function getStudent(): Student
    {
        return $this->student;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
}
