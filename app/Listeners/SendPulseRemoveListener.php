<?php

namespace App\Listeners;

use App\Events\SendExceptionEvent;
use App\Events\SendPulseRemoveEvent;
use App\Service\SendPulse\SendPulseService;
use App\Service\UpdateStudentNotificationService;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendPulseRemoveListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param SendPulseRemoveEvent $event
     * @return void
     */
    public function handle(SendPulseRemoveEvent $event)
    {
        try
        {
            $student = $event->getStudent();
            $service = new SendPulseService();
            $bookTypes = $event->getBookTypes();

            if($bookTypes){
                foreach ($bookTypes as $bookType){
                    if(issetInSendPulse($student, $bookType)){
                        $responseApi = $service->removeUser($student->email, $bookType);
                        if ($responseApi->result == true){
                            $updateNotificationSettingService = new UpdateStudentNotificationService($student);
                            $updateNotificationSettingService->updateFieldByBookType($bookType, 0);
                        }
                    }

                }
            }


        }catch (\Exception $exception)
        {
            event(new SendExceptionEvent($exception));
        }
    }
}
