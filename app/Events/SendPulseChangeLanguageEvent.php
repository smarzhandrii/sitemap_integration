<?php

namespace App\Events;

use App\Models\Student;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class SendPulseChangeLanguageEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    protected $student;
    protected $lang;
    protected $bookTypes;


    /**
     * SendPulseChangeStatusEvent constructor.
     *
     * @param $params
     */
    public function __construct($params)
    {
        $this->student = $params->student;
        $this->lang = (!empty($params->lang)) ? $params->lang : $this->student->getNotificationLanguage();
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }

    /**
     * @return Student
     */
    public function getStudent(): Student
    {
        return $this->student;
    }

    /**
     * @return string
     */
    public function getLang()
    {
        return $this->lang;
    }

    /**
     * @return array
     */
    public function getBookTypes()
    {
        return $this->bookTypes;
    }
}
