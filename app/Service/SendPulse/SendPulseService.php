<?php

namespace App\Service\SendPulse;

use App\Events\SendPulseChangeLanguageEvent;
use App\Models\Student;
use Illuminate\Support\Facades\Log;
use Sendpulse\RestApi\Storage\FileStorage;

class SendPulseService
{

    private $id = '';
    private $key = '';
    private $logger;

    protected $langLabels;
    protected $addressBookIds;
    protected $bookId;

    /**
     * @var ApiClient
     */
    public  $SPApiClient;

    public function __construct()
    {
        $this->id = config('sendpulse.apiID');
        $this->key = config('sendpulse.apiKey');
        $this->logger = Log::channel('sendpulse_response');
        $this->addressBookIds = config('sendpulse.address_book_ids');
        $this->langLabels = config('app.available_locales_labels');
        $this->SPApiClient = new ApiClient($this->id, $this->key, new FileStorage());
    }

    /**
     * @param $name
     * @param $email
     * @param string $lang
     * @param string $type
     * @return mixed
     */
    public function subscribeUser($name, $email, $lang = 'ua', $type = 'subscribe')
    {
        $bookId = $this->getAddressBookId($type, $lang);
        $response = $this->SPApiClient->addEmails($bookId, [
            [
                'email' => $email,
                'variables' => [
                    'name' => $name,
                    'language' => $this->getLanguageLabel($lang)
                ]
            ]
        ]);
        $this->logResponse('Subscribe '.$type, $bookId, $email, json_encode($response));

        return $response;
    }

    /**
     * @param $email
     * @param string $lang
     * @param string $type
     * @return mixed
     */
    public function doubleOptInSubscribeUser($email, $lang = 'ua', $type = 'subscribe')
    {
        $bookId = $this->getAddressBookId($type, $lang);
        $senderEmail = $this->getFirstActiveSender();
        $response = $this->SPApiClient->addEmails($bookId, [
            [
                'email' => $email,
            ]
        ], [
            'confirmation' => 'force',
            "sender_email" => $senderEmail,
            "message_lang" => $lang
        ]);
        $this->logResponse('Subscribe Double Opt In'.$type, $bookId, $email, json_encode($response));

        return $response;
    }

    public function getBlackList()
    {
        return $this->SPApiClient->getBlackList();
    }

    /**
     * @param $email
     * @return \stdClass
     */
    public function removeFromBlackList($email)
    {
        $response = $this->SPApiClient->removeFromBlackList($email);
        $this->logResponse('Remove From Black List ', '', $email, json_encode($response));

        return $response;

    }

    /**
     * @param $email
     * @param $type
     * @param $lang
     * @return \stdClass
     */
    public function updateUserLanguage($email, $type, $lang)
    {
        $bookId = $this->getAddressBookId($type);
        $response = $this->SPApiClient->updateEmailVariables($bookId, $email, [
            'language' => $this->getLanguageLabel($lang)
        ]);
        $this->logResponse('Update '.$type, $bookId, $email, json_encode($response));

        return $response;
    }

    /**
     * @param $email
     * @param $type
     * @param $name
     * @return \stdClass
     */
    public function updateUserName($email, $type, $name)
    {
        $bookId = $this->getAddressBookId($type);
        $response = $this->SPApiClient->updateEmailVariables($bookId, $email, [
            'name' => $name
        ]);
        $this->logResponse('Update '.$type, $bookId, $email, json_encode($response));

        return $response;
    }



    /**
     * @param $email
     * @param string $type
     * @return mixed
     */
    public function removeUser($email, $type = 'subscribe')
    {
        $bookId = $this->getAddressBookId($type);
        $response = $this->SPApiClient->removeEmails($bookId, [$email]);
        $this->logResponse('Remove '.$type, $bookId, $email, json_encode($response));

        return $response;
    }

    /**
     * @param $email
     * @param string $type
     * @return mixed
     */
    public function unsubscribeUser($email, $type = 'subscribe')
    {
        $bookId = $this->getAddressBookId($type);
        $response = $this->SPApiClient->unsubscribeEmails($bookId, [$email]);
        $this->logResponse('Unsubscribe '.$type, $bookId, $email, json_encode($response));

        return $response;
    }


    /**
     * @param $email
     * @param string $type
     * @return mixed
     */
    public function deleteUser($email, $type = 'black_friday')
    {
        $bookId = $this->getAddressBookId($type);
        $response = $this->SPApiClient->deleteEmails($bookId, [$email]);
        $this->logResponse('Delete '.$type, $bookId, $email, json_encode($response));

        return $response;
    }

    /**
     * @param $email
     * @param string $type
     * @return \stdClass
     */
    public function getEmailInfo($email, $type = 'subscribe')
    {
        $bookId = $this->getAddressBookId($type);
        $response = $this->SPApiClient->getEmailInfo($bookId, $email);
        $this->logResponse('GetEmailInfo '.$type, $bookId, $email, json_encode($response));

        return $response;
    }

    /**
     * @return mixed
     */
    public function getListAddressBooks()
    {
        $response = $this->SPApiClient->listAddressBooks();
        $this->logResponse('GetListAddressBooks ', '', '', json_encode($response));

        return $response;
    }

    /**
     * @param $bookId
     * @return bool|\stdClass
     */
    public function getBookInfo($bookId)
    {
        if($bookId){
            $response = $this->SPApiClient->getBookInfo($bookId);
            $this->logResponse('GetBookInfo ', '', '', json_encode($response));

            return $response;
        }

        return false;
    }

    /**
     * @param $bookId
     * @return bool|\stdClass
     */
    public function getBookVariables($bookId)
    {
        if($bookId){
            $response = $this->SPApiClient->getBookVariables($bookId);
            $this->logResponse('GetBookVariables ', '', '', json_encode($response));

            return $response;
        }

        return false;
    }

    /**
     * @param Student $student
     * @return array|bool
     */
    public function getStudentAddressBooks(Student $student)
    {
        $notificationSettings = $student->studentNotificationSetting;

        $addressBooks = [];
        if ($notificationSettings->is_subscribes && issetInSendPulse($student, 'subscribe'))
            $addressBooks[] = 'subscribe';

        if ($notificationSettings->is_subscribes && issetInSendPulse($student, 'subscribe_register'))
            $addressBooks[] = 'subscribe_register';

        if ($notificationSettings->is_news && issetInSendPulse($student, 'digest'))
            $addressBooks[] = 'digest';

        if ($notificationSettings->is_promotions && issetInSendPulse($student, 'promotions'))
            $addressBooks[] = 'promotions';

        if ($notificationSettings->is_new_courses && issetInSendPulse($student, 'new_courses'))
            $addressBooks[] = 'new_courses';

        if(!empty($addressBooks)){
            return $addressBooks;
        }

        return false;
    }

    protected function getFirstActiveSender()
    {
        $listSenders = $this->getListSenders();
        if(is_array($listSenders)){
            foreach ($listSenders as $key => $sender){
                if ($sender->status == 'Active')
                    return $sender->email;
            }
        }

        return false;
    }

    protected function getListSenders()
    {
        return $this->SPApiClient->listSenders();
    }

    /**
     * @param string $type
     * @return mixed
     */
    protected function getAddressBookId($type = 'subscribe')
    {
        return $this->addressBookIds[$type];
    }

    /**
     * @param string $lang
     * @return mixed|string
     */
    protected function getLanguageLabel($lang = 'ua')
    {
        return (isset($this->langLabels[$lang])) ? $this->langLabels[$lang] : 'УКР';
    }

    /**
     * @param string $action
     * @param string $addressBookId
     * @param string $email
     * @param string $response
     * @return bool
     */
    protected function logResponse($action = '', $addressBookId = '', $email ='', $response = '')
    {
        $this->logger->info($action.' '.$addressBookId.' '.$email.': '.json_encode($response));

        return true;
    }


}
