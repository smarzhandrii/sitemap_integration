<?php

namespace App\Listeners;

use App\Events\SendExceptionEvent;
use App\Events\SendPulseChangeLanguageEvent;
use App\Models\Student;
use App\Service\SendPulse\SendPulseService;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendPulseChangeLanguageListener
{
    /**
     * SendPulseChangeStatusListener constructor.
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param SendPulseChangeLanguageEvent $event
     * @return void
     */
    public function handle(SendPulseChangeLanguageEvent $event)
    {
        try
        {
            $student = $event->getStudent();
            $service = new SendPulseService();
            $bookTypes = $service->getStudentAddressBooks($student);

            if($bookTypes){
                foreach ($bookTypes as $bookType){
                    $service->updateUserLanguage($student->email, $bookType, $event->getLang());
                }
            }

        }catch (\Exception $exception)
        {
            event(new SendExceptionEvent($exception));
        }
    }
}
