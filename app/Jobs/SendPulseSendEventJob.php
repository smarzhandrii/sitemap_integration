<?php

namespace App\Jobs;

use App\Events\SendExceptionEvent;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class SendPulseSendEventJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 3;

    protected $event;
    protected $params;

    /**
     * Create a new job instance.
     *
     * SendPulseAddEvent constructor.
     * @param $eventName
     * @param $params
     *
     */
    public function __construct($eventName, $params)
    {
        $this->event = $eventName;
        $this->params = (object)$params;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try
        {
            event(new $this->event($this->params));

        }catch (\Exception $exception)
        {
            event(new SendExceptionEvent($exception));
        }
    }
}
