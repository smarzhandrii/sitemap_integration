<?php

namespace App\Http\Controllers;

use App\Events\SendPulseAddEvent;
use App\Helpers\ResponseJsonError;
use App\Http\Requests\Subscribe\SubmitRequest;
use App\Integration\Mailchimp\Mailchimp;
use App\Jobs\SendPulseSendEventJob;
use App\Mail\RegistrationContinue;
use App\Models\EventView;
use App\Models\Student;
use App\Models\Subscriber;
use App\Service\SendPulse\SendPulseService;
use App\Service\UpdateStudentNotificationService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Response;

class SubscribeController extends Controller
{
    public function news(SubmitRequest $request)
    {
        $email = $request->get('email');
        $name = $request->get('name');

        try
        {
            if (auth()->guest()){
                $countStudents = Student::where('email', $email)->count();
                if ($countStudents < 1)
                {
                    $subscriber = Subscriber::where('email', $email)->first();
                    if(!$subscriber){
                        $subscriber = Subscriber::create([
                            'email' => $email,
                            'name' => $name,
                            'code' => md5($email . time()),
                        ]);
                    }

                    if(is_null($subscriber->code)){
                        $subscriber->code = md5($subscriber->email . time());
                        $subscriber->save();
                    }

                    Mail::to($subscriber->email)->send(new RegistrationContinue($subscriber));

                    $response['success'] = [
                        'result' => Lang::get('messages.subscribeSuccess'),
                        'modal_id' => 'modal_subscrib-success'
                    ];
                }else{
                    $response['success']['modal_id'] = 'modal_subscrib-need-auth';
                }
            }else{
                if (auth()->user()->is_subscribe) {
                    $response['success']['modal_id'] = 'modal_subscrib-exists';
                }else{
                    $response['success']['redirect'] = route('office_notifications');

                    dispatch(new SendPulseSendEventJob(SendPulseAddEvent::class, [
                        'student' => auth()->user(),
                        'bookTypes' => ['subscribe']
                    ]))->onQueue('sendpulse');
                }
            }

            return Response::json($response);
        } catch (\Exception $exception)
        {
            return ResponseJsonError::factory($exception)->getResponse();
        }
    }

    public function confirm($code, Request $request)
    {
        $subscriber = Subscriber::where('code', $code)->first();

        if (!$subscriber->id)
        {
            abort(404);
        }

        $subscriber->is_confirm = 1;
        $subscriber->code = null;
        $subscriber->save();

        return view('public.subscribe.confirm');
    }

    public function getSendPulseHooks(Request $request)
    {
        Log::channel('sendpulse_response')->info(json_encode($request));
        $data = $_REQUEST;

        $data['data'] = json_decode(file_get_contents('php://input'));
        $hookInfo = $data['data'];
        Log::channel('sendpulse_response')->info('WebHook '.json_encode($data));
        if(is_array($hookInfo) && !empty($hookInfo)){
            foreach ($hookInfo as $eventInfo) {
                if(isset($eventInfo->book_id) && $this->searchAddressBook($eventInfo->book_id) !== 'black_friday'){
                    switch ($eventInfo->event){
                        case "new_emails":
                            $bookName = $this->searchAddressBook($eventInfo->book_id);

                            if(!$bookName){
                                Log::channel('sendpulse_response')->info('WebHookInfo Undefined book: ' . $eventInfo->book_id);
                            }else{
                                Log::channel('sendpulse_response')->info('WebHookInfo Add new user: ' . json_encode($eventInfo));

                                $this->searchStudentAndUpdateNotificationSetting($eventInfo->variables->email, $bookName, 1);
                            }

                            break;
                        case "unsubscribe":
                        case "delete":
                            if(isset($eventInfo->categories) && !empty($eventInfo->categories)){
                                foreach ($eventInfo->categories as $category){
                                    $bookName = $this->searchAddressBookByCategory($category);

                                    if(!$bookName){
                                        Log::channel('sendpulse_response')->info('WebHookInfo Undefined category: ' . $category);
                                        continue;
                                    }

                                    $this->searchStudentAndUpdateNotificationSetting($eventInfo->email, $bookName, 0);
                                }
                            }else{
                                $bookName = $this->searchAddressBook($eventInfo->book_id);

                                if(!$bookName){
                                    Log::channel('sendpulse_response')->info('WebHookInfo Undefined book: ' . $eventInfo->book_id);
                                }else{
                                    $this->searchStudentAndUpdateNotificationSetting($eventInfo->email, $bookName, 0);
                                }

                            }

                            break;
                    }
                }
            }
        }

        return '';
    }

    /**
     * @param $bookId
     * @return bool|int|mixed|string
     */
    protected function searchAddressBook($bookId){
        $configBooks = config('sendpulse.address_book_ids');

        return array_search($bookId, $configBooks);
    }

    /**
     * @param $categoryName
     * @return bool|mixed
     */
    protected function searchAddressBookByCategory($categoryName)
    {
        $configBooks = config('sendpulse.categories');

        if (isset($configBooks[$categoryName])){
            return $configBooks[$categoryName];
        }

        return false;
    }

    /**
     * @param string $email
     * @param string $bookName
     * @param int $newNotificationSettingValue
     * @return bool
     */
    protected function searchStudentAndUpdateNotificationSetting($email, $bookName, $newNotificationSettingValue){
        $student = Student::where('email', $email)->first();
        if ($student){
            $updateNotificationSettingService = new UpdateStudentNotificationService($student);
            if ($updateNotificationSettingService->updateFieldByBookType($bookName, $newNotificationSettingValue))
                Log::channel('sendpulse_response')->info('WebHookInfo Update user: '.$email.' '.$bookName.' '.$newNotificationSettingValue);
            else
                Log::channel('sendpulse_response')->info('WebHookInfo Failed update user: '.$email.' '.$bookName.' '.$newNotificationSettingValue);

        }else{
            Log::channel('sendpulse_response')->info('WebHookInfo Student not found: '.$email.' '.$bookName.' '.$newNotificationSettingValue);
        }

        return true;
    }
}
